# Template de Automação de API com RestAssured e TestNG

> **Status:** 🚧 Em construção 🚧
> 
## Visão Geral

Este projeto serve como um template para automação de testes de API, utilizando RestAssured para fazer requisições HTTP e TestNG para gerenciar os casos de teste. O design do projeto permite a utilização de múltiplas base URLs e suporta o gerenciamento de diferentes ambientes de execução (como desenvolvimento, QA, e produção) através de configurações flexíveis. Este template é ideal para equipes que buscam padronizar e escalar suas práticas de automação de testes de API.

## Tecnologias Utilizadas

- **Maven**: Ferramenta de gerenciamento e compreensão de projeto de software.
- **Java 17+**: Linguagem de programação usada para desenvolver os testes.
- **RestAssured**: Biblioteca Java para testar serviços REST.
- **TestNG**: Framework de teste utilizado para organizar e executar os casos de teste.
- **Lombok**: Biblioteca Java que ajuda a reduzir o boilerplate code.
- **YAML**: Usado para definir as configurações dos ambientes.

## Configuração do Projeto

Para começar a utilizar este template, siga os passos abaixo:

1. Clone o repositório para o seu ambiente local.
2. Instale o Java 17 ou superior e o Maven.
3. Configure os arquivos de ambiente `application-<env>.yml` de acordo com os seus ambientes de teste. Por padrão, o projeto já vem com arquivos de configuração para ambientes `dev` e `qa`.
4. Utilize o Maven para gerenciar as dependências e executar os testes.

## Executando Testes

Para executar os testes em um ambiente específico, use o comando Maven abaixo, substituindo `<env>` pelo ambiente desejado (por exemplo, `dev`, `qa`):

```bash
mvn test -Denv=<env>
