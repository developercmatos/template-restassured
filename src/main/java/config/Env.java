package config;

public class Env {
    public static String getEnv() {
        if(System.getProperty("env") == null) {
            return "dev";
        }
        switch (System.getProperty("env")) {
            case "dev":
                return "dev";
            case "qa":
                return "qa";
            default:
                throw new RuntimeException("Ambiente não configurado.");
        }
    }
}
