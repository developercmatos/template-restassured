package config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;


import java.io.InputStream;
import java.util.Map;

import static config.Env.getEnv;

public class EnvironmentConfig {

    private static Map<String, String> servicesConfig;

    static {
        loadConfig();
    }

    private static void loadConfig() {
        String env = Env.getEnv();
        String configFile = String.format("application-%s.yml", env);
        try (InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFile)) {
            if (is == null) {
                throw new RuntimeException("Não foi possível encontrar o arquivo de configuração: " + configFile);
            }
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            Map<String, Map<String, String>> config = mapper.readValue(is, Map.class);
            servicesConfig = config.get("services");
        } catch (Exception e) {
            throw new RuntimeException("Falha ao carregar a configuração do ambiente", e);
        }
    }

    public static String getServiceUrl(String serviceName) {
        return servicesConfig.getOrDefault(serviceName, "URL not defined");
    }
}
